import csv
from genericpath import isdir
import requests

import os
import glob
from zipfile import ZipFile
from datetime import datetime as dt
from datetime import date,timedelta
import constants

def sortStringDateList(dateStringList,dateFormat):
    dateStringList.sort(key=lambda date: dt.strptime(date, dateFormat))

def removeFilesUsingPattern(filePath,pattern):
        fileList = glob.glob(filePath+pattern)
        for file in fileList:
            try:
                os.remove(file)
            except:
                print("Error while deleting the file : "+str(file))

def getCurrentDateInIndianFormat():
    currentDate  = date.today()
    formattedDate  = currentDate.strftime("%d-%m-%Y") 
    return formattedDate
    

class DowloadUtilities(object):
    def __init__(self) -> None:
        self.checkInputFolderExistance()
        self.checkOutputFolderExistance()

    
    def checkInputFolderExistance(self):
        folderPath  = constants.OPEN_INTREST_INPUT_FOLDER
        isExists = os.path.isdir(folderPath)
        if not isExists:
            os.makedirs(folderPath)
    
    def checkOutputFolderExistance(self):
        folderPath  = constants.OPEN_INTREST_OUTPUT_FOLDER
        isExists = os.path.isdir(folderPath)
        if not isExists:
            os.makedirs(folderPath)

    def downloadReport(self,baseUrl,reportPath,fileName):
        isSuccessFul = False
        url = baseUrl+fileName
        print("Nse ReportUrl = "+str(url))
        response = requests.get(url)
        statusCode = response.status_code
        print("Response from NSE site = "+str(statusCode))
        if statusCode == 200 :
            finalFileNameWithPath = reportPath+fileName
            open(finalFileNameWithPath,'wb').write(response.content)
            isSuccessFul = True
        return isSuccessFul
    


    def openZip(self,inputfilePath,outputFilePath,fileName):
        finalPath = inputfilePath+fileName
        with ZipFile(finalPath,'r') as zip:
            #print("Extracting all files.")
            zip.extractall(path=outputFilePath)
            #print("Extracted all files from zip successfully!")

    def removeUneccessaryFiles(self,filePath,fileName):
        os.remove(filePath+fileName) 

    def calculateDate(self,days):
        currentDate  = date.today()
        yesterdayDate = currentDate - timedelta(days)
        formattedDate  = yesterdayDate.strftime("%d%m%Y") 
        
        return formattedDate


if __name__ == "__main__":
    downloadUtils = DowloadUtilities()
    downloadUtils.checkInputFolderExistance()
    downloadUtils.checkOutputFolderExistance()



