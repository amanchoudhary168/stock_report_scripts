import datetime
import glob
import constants
import csv
from report_utilities import DowloadUtilities
import report_utilities as Util




def startProcessing():
    print("Started the process")
    downloadFiles()
    generateReport()
    Util.removeFilesUsingPattern(constants.OPEN_INTREST_OUTPUT_FOLDER,constants.OPEN_INTREST_INVALID_FILE_EXTN)

    

def downloadFiles():
    fileCount = 0
    daysDelta = 1
    reportUtils = DowloadUtilities()
    while fileCount < constants.OPEN_INTREST_TIMEFRAME_DAYS_COUNT:
        fileName = reportUtils.calculateDate(daysDelta)+".zip"
        downloadStat  = reportUtils.downloadReport(constants.OPEN_INTREST_BASE_URL,constants.OPEN_INTREST_INPUT_FOLDER,fileName)
        if downloadStat:
            fileCount = fileCount+1
            reportUtils.openZip(constants.OPEN_INTREST_INPUT_FOLDER,constants.OPEN_INTREST_OUTPUT_FOLDER,fileName)
            reportUtils.removeUneccessaryFiles(constants.OPEN_INTREST_INPUT_FOLDER,fileName)
        daysDelta = daysDelta+1
    Util.removeFilesUsingPattern(constants.OPEN_INTREST_OUTPUT_FOLDER,constants.OPEN_INTREST_INVALID_FILE_EXTN)


def readCsvFile(inputFile,optionDict):
    with open(inputFile,'r') as inputcsvFile:
        reader = csv.DictReader(inputcsvFile, delimiter=',', quotechar='|')
        for row in reader:
            subDetails = {}
            for key in row.keys():
                subDetails[key.strip()] = row[key]
            
            dataTuple = (subDetails['MWPL'],subDetails['NSE Open Interest'],subDetails['Scrip Name'])
            if subDetails['ISIN'] in optionDict.keys():
                optionDict[subDetails['ISIN']][subDetails['Date']] = dataTuple 
            else:
                optionDict[subDetails['ISIN']] = {}
                optionDict[subDetails['ISIN']][subDetails['Date']] = dataTuple
    inputcsvFile.close()

def getOutputFileName():
    currentDate = Util.getCurrentDateInIndianFormat()
    finalFileName = constants.OPEN_INTREST_REPORT_FOLDER+constants.OPEN_INTREST_OUTPUT_FILE_NAME+currentDate+constants.OPEN_INTREST_OUTPUT_FILE_EXTN
    return finalFileName

def generateReport():
    print('Report Generation Started ..')
    inputFilesList = glob.glob(constants.OPEN_INTREST_OUTPUT_FOLDER+constants.OPEN_INTREST_INPUT_FILE_EXTN)
   
    #outputFile = csv.DictWriter(constants.OPEN_INTREST_OUTPUT_FOLDER+"oi_details.csv",fieldnames=filedNames)
    stockRecords = {}
    for inputFile in inputFilesList:
        print('Input File :'+str(inputFile))
        readCsvFile(inputFile,stockRecords)

    isinList = list(stockRecords)
    dateList = list(stockRecords[isinList[0]])
    Util.sortStringDateList(dateList,"%d-%b-%Y")
    dateListExhausted = [x+"(Exhausted%)" for x in dateList ]
    print(dateListExhausted)
      
    fieldNames = ['Name','Total Units']
    fieldNames.extend(dateList)
    fieldNames.extend(dateListExhausted)
    fieldNames.append('Trend')
    print(fieldNames)
    
    outputcsvfile = open(getOutputFileName(), 'w')
    writer = csv.DictWriter(outputcsvfile,fieldNames)
    writer.writeheader()
    for isin in isinList:
        row = {}
        stockDetails = stockRecords[isin]
        stockDetailsTuple = stockDetails[dateList[0]]

        row['Name'] = stockDetailsTuple[2]
        row['Total Units'] = stockDetailsTuple[0]
        trend = "Up"
        for dateData in dateList:
            row[dateData] = stockDetails[dateData][1]
            exhaustedData = round((int(row[dateData])*100)/int(row['Total Units']),2)
            row[dateData+"(Exhausted%)"] = exhaustedData
        prevData = 0
        for dateData in dateList:
            if prevData > int(stockDetails[dateData][1]) :
                trend = "Down"
                break
            prevData = int(stockDetails[dateData][1])
        row["Trend"] = trend
        writer.writerow(row)
    outputcsvfile.close()

    
        
        
    

if __name__ == "__main__":
    startProcessing()

